const CountReducer = (state = {
    count: 0,
    wishCount: 0
}, action) => {
    const count = state.count;
    const wishCount = state.wishCount;
    switch(action.type){
        case 'increase': 
            return {
                count: Number(count) + 1,
                wishCount: Number(count) + 1
            }
        case 'update': 
            return {
                count: Number(action.wishCount)
            }
        default:
            return state
    }
};

export default CountReducer;