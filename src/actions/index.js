export const increaseTodo = () => {
    return {
        type: 'increase',
    }
}

export const updateTodo = (wishCount) => {
    return {
        type: 'update',
        wishCount: wishCount,
    }
}