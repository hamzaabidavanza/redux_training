import {connect} from 'react-redux';
import Counter from '../components/Counter';
import * as actions from '../actions'


// Map Redux state to component props
const mapStateToProps = (state) => {
    return {
        count: state.CountReducer.count,
        wishCount: state.CountReducer.wishCount
    }
}

// Map Redux actions to component props
const mapDispatchToProps = (dispatch) => {
    debugger;
    return {
        onIncreaseClick: () => dispatch(actions.increaseTodo()),
        onUpdateClick: event => dispatch((
            actions.updateTodo(event.target.value)
            )),
    }
}

// Connected Component
const VisibleCounter = connect(
    mapStateToProps,
    mapDispatchToProps
)(Counter);

export default VisibleCounter;