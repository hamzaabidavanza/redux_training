import React, {Component} from 'react';

class Counter extends Component{
    render(){
        const {
            count,
            wishCount,
            onIncreaseClick,
            onUpdateClick
        } = this.props;

        return(
            <div>
                <span>{count}</span>
                <button onClick={onIncreaseClick}>Increase</button>
                <input value={wishCount} type='text' onChange={onUpdateClick}></input>
            </div>
        );
    }
}

export default Counter;